"""
This module provides a web application with a WebSocket interface for interacting with the voice assistant.

The application serves a web page that establishes a WebSocket connection and facilitates
the exchange of messages between the client and the voice assistant.
"""

# Standard library imports
import json

# Third-party library imports
from bottle import Bottle, run, template, static_file, request, abort
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer

# Local library imports
from voice_chat.voice_assistant import voice_chat_loop
from voice_chat.utils import log_error

app = Bottle()


@app.route("/")
def index():
    """
    Render the index.html template for the web application.

    Returns:
        str: The rendered HTML template.
    """
    try:
        return template("index", template_lookup=["./templates"])
    except Exception as e:
        log_error(f"Error rendering index template: {str(e)}")


@app.route("/static/<filepath:path>")
def serve_static(filepath):
    """
    Serve static files (CSS, JavaScript, etc.) for the web application.

    Args:
        filepath (str): The path to the static file to serve.

    Returns:
        Static file content.
    """
    try:
        return static_file(filepath, root="./static")
    except Exception as e:
        log_error(f"Error serving static file: {str(e)}")


@app.route("/websocket")
def handle_websocket():
    """
    Handle the WebSocket connection for the voice assistant.

    This function establishes a WebSocket connection and manages the communication
    between the client and the voice assistant. It receives messages from the client,
    passes them to the voice assistant, and sends the assistant's responses back to the client.
    """
    wsock = request.environ.get("wsgi.websocket")
    if not wsock:
        abort(400, "Expected WebSocket request.")

    try:
        chat_loop = voice_chat_loop(wsock)
        next(chat_loop)
        while True:
            message = wsock.receive()
            if message is None:
                break
            event = chat_loop.send(message)
            while event:
                wsock.send(event)
                event = next(chat_loop)

    except WebSocketError:
        pass
    except StopIteration:
        pass
    except Exception as e:
        log_error(f"Error handling WebSocket: {str(e)}")


if __name__ == "__main__":
    try:
        server = WSGIServer(("localhost", 8080), app, handler_class=WebSocketHandler)
        server.serve_forever()
    except Exception as e:
        log_error(f"Error starting server: {str(e)}")
