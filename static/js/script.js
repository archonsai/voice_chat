let socket

function init () {
  socket = new WebSocket('ws://' + window.location.host + '/websocket')
  const chatContainer = document.getElementById('chatContainer')
  let transcriptionCount = 0
  let responseCount = 0
  const ringBufferSize = 10
  let ringBuffer = ''

  socket.onmessage = function (event) {
    const data = JSON.parse(event.data)

    if (data.type === 'transcription') {
      // Handle transcription data (unchanged)
      const userTextElement = document.createElement('div')
      userTextElement.id = `transcription-${transcriptionCount}`
      userTextElement.className = 'speech-bubble now me'
      userTextElement.textContent = data.data
      chatContainer.appendChild(userTextElement)

      // Animate the user text bubble
      gsap.from(userTextElement, {
        opacity: 0,
        x: 100,
        duration: 1,
        ease: 'power2.out'
      })

      transcriptionCount++
    } else if (data.type === 'response') {
      let responseElement = document.getElementById(`response-${responseCount}`)

      if (!responseElement) {
        responseElement = document.createElement('div')
        responseElement.id = `response-${responseCount}`
        responseElement.className = 'speech-bubble now'
        responseElement.style.opacity = 0
        responseElement.style.transform = 'translateX(-100px)'
        chatContainer.appendChild(responseElement)

        // Animate the response bubble
        gsap.to(responseElement, {
          opacity: 1,
          x: 0,
          duration: 1,
          ease: 'power2.out'
        })
      }

      if (data.data === '<|endofresponse|>') {
        responseCount++
      } else {
        ringBuffer += data.data
        if (ringBuffer.length > ringBufferSize) {
          ringBuffer = ringBuffer.slice(-ringBufferSize)
        }

        if (ringBuffer.includes('<|par|>')) {
          const responseParts = ringBuffer.split('<|par|>')
          ringBuffer = ''

          if (responseParts.length > 1) {
            // Modify existing speech-bubble form and content
            const original = document.getElementById(
              `response-${responseCount}`
            )
            const original_text = original.textContent
            original.className = 'speech-bubble'
            original.textContent = original_text.replace(/<\|par\|/g, '')

            // New speech-bubble at par delimiter
            responseCount++
            responseElement = document.createElement('div')
            responseElement.id = `response-${responseCount}`
            responseElement.className = 'speech-bubble now'
            responseElement.style.opacity = 0
            responseElement.style.transform = 'translateX(-100px)'
            responseElement.textContent = responseParts[1].replace(
              /<\|par\|>/g,
              ''
            )
            chatContainer.appendChild(responseElement)

            // Animate the new response bubble
            gsap.to(responseElement, {
              opacity: 1,
              x: 0,
              duration: 1,
              ease: 'power2.out'
            })
          }
        } else {
          responseElement.textContent += data.data
        }
      }

      // Scroll to the bottom of the chat container
      chatContainer.scrollTop = chatContainer.scrollHeight
    } else if (data.type === 'create') {
      const processBubble = document.createElement('div')
      processBubble.id = data.data.element_id
      processBubble.className = 'process-bubble'
      processBubble.textContent = data.data.label
      chatContainer.appendChild(processBubble)

      // Animate the progress bubble
      gsap.from(processBubble, {
        opacity: 0,
        y: 50,
        duration: 1,
        ease: 'power2.out'
      })
    } else if (data.type === 'update') {
      const updatedElement = document.getElementById(data.data.element_id)
      if (updatedElement) {
        for (const key in data.data) {
          if (key !== 'element_id') {
            if (key === 'display') {
              updatedElement.style.display = data.data[key]
            } else if (key === 'textContent') {
              updatedElement.textContent = data.data[key]
            } else if (key === 'className') {
              updatedElement.className = data.data[key]
            } else if (updatedElement.hasOwnProperty(key)) {
              updatedElement[key] = data.data[key]
            } else {
              console.warn(
                `Property ${key} not found in element with id ${data.data.element_id}`
              )
            }
          }
        }
      } else {
        console.warn(`Element with id ${data.data.element_id} not found`)
      }
    }
  }

  let isSpaceBarPressed = false

  document.addEventListener('keydown', function (event) {
    if (event.code === 'Space' && !isSpaceBarPressed) {
      isSpaceBarPressed = true
      socket.send(JSON.stringify({ type: 'signal', data: 'start' }))
      console.log('start')
    }
  })

  document.addEventListener('keyup', function (event) {
    if (event.code === 'Space' && isSpaceBarPressed) {
      isSpaceBarPressed = false
      socket.send(JSON.stringify({ type: 'signal', data: 'stop' }))
      console.log('stop')
    }
  })

  const voiceOutputToggle = document.getElementById('voice-output-toggle')
  const modelInput = document.getElementById('model-input')

  voiceOutputToggle.addEventListener('change', () => {
    socket.send(
      JSON.stringify({
        type: 'setting',
        name: 'speech_on',
        value: voiceOutputToggle.checked
      })
    )
  })

  modelInput.addEventListener('input', () => {
    socket.send(
      JSON.stringify({
        type: 'setting',
        name: 'configured_model',
        value: modelInput.value
      })
    )
  })
}

document.addEventListener('DOMContentLoaded', init)
