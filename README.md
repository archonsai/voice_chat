# Voice Talk

Voice Talk is a Python script that enables voice-based conversations with an AI assistant. It utilizes the Anthropic API for text generation and the ElevenLabs API for text-to-speech. The conversation is recorded using the microphone, transcribed using the Whisper model, and the AI-generated responses are played back using the ElevenLabs API.

## Features

- Voice-based conversation with an AI assistant
- Real-time transcription of user's speech using the Whisper model
- Streaming of AI-generated responses using the Anthropic API
- Text-to-speech conversion of AI responses using the ElevenLabs API

## Dependencies

- [Anthropic API](https://www.anthropic.com/) - For generating AI responses
- [ElevenLabs API](https://www.elevenlabs.io/) - For text-to-speech conversion
- [faster-whisper](https://github.com/guillaumekln/faster-whisper) - For real-time speech transcription

## Installation

1. Clone the repository:

    ```bash
    git clone <https://github.com/your-username/voice-talk.git>
    ```

2. Install the required dependencies:

    ```bash
    pip install anthropic elevenlabs faster-whisper pyaudio
    ```

3. Set up the API keys:

    - Sign up for an Anthropic API key at [https://www.anthropic.com/](https://www.anthropic.com/)
    - Sign up for an ElevenLabs API key at [https://www.elevenlabs.io/](https://www.elevenlabs.io/)
    - Set the `ANTHROPIC_API_KEY` environment variable with your Anthropic API key
    - Replace the `eleven_client` API key in the code with your ElevenLabs API key

4. Run the script:

    ```bash
    python voice_talk.py
    ```

5. [OPTIONAL] Install `mpv`

    This will be required for streaming the audio output by the `` library. You can install it on mac using *homebrew*.

    ```bash
    brew install mpv
    ```

## Usage

1. Run the `voice_talk.py` script.
2. Press the spacebar to start recording your voice.
3. Speak your message and press the spacebar again to stop recording.
4. The script will transcribe your speech and generate a response from the AI assistant.
5. The AI-generated response will be played back using text-to-speech.
6. Repeat steps 2-5 to continue the conversation.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgements

- [Anthropic](https://www.anthropic.com/) for providing the AI text generation API
- [ElevenLabs](https://www.elevenlabs.io/) for providing the text-to-speech API
- [faster-whisper](https://github.com/guillaumekln/faster-whisper) for the real-time speech transcription library
- [Semantic Versioning](https://semver.org/spec/v2.0.0.html) used in this project
- Microphone icon by [Freepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/free-icons/podcast "podcast icons").
