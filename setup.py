from setuptools import setup, find_packages

setup(
    name="voice_interface",
    version="0.1.0",
    packages=find_packages(),
    install_requires=[
        "pyaudio",
        "numpy",
        "openai",
        "elevenlabs",
        "wave",
        "keyboard",
        "faster-whisper",
        "anthropic",
        "python-dotenv",
    ],
)
