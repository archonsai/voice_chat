"""
This module provides a class for recording and listening to audio from a microphone.

The Listener class handles audio recording, voice activity detection, and optional
transcription using a specified transcriber. It supports background noise removal,
threshold-based voice activation, and callback functions for transcription events.
"""

# Standard library imports
import threading
import wave
import time
from collections import deque
import logging

# Third-party library imports
import pyaudio
import numpy as np

# Local library imports
from voice_chat.utils import log_info, log_error, log_warning


class Listener:
    def __init__(
        self,
        audio_file="audio_file.wav",
        background=0,
        threshold=15,
        pause_length=0.5,
        buffer_size=16384,
        chunk_size=1024,
        transcriber=None,
        transcriber_callback=None,
    ):
        """
        Initialize the Listener object.

        Args:
            audio_file (str): Path to the audio file to save the recorded audio.
            background (int): Background noise level in decibels.
            threshold (int): Voice activation threshold in decibels.
            pause_length (float): Minimum pause length in seconds to detect end of speech.
            buffer_size (int): Maximum buffer size in bytes for audio data.
            chunk_size (int): Chunk size in bytes for reading audio data.
            transcriber (Transcriber): Transcriber object for speech-to-text conversion.
            transcriber_callback (callable): Callback function for transcription events.
        """
        self.audio_file = audio_file
        self.record_active = False
        self.listen_active = False
        self.voice_active = False
        self.audio_buffer = deque(maxlen=buffer_size // chunk_size)
        self.stream = None
        self.audio = pyaudio.PyAudio()
        self.background = background
        self.threshold = threshold
        self.pause_length = pause_length
        self.buffer_size = buffer_size
        self.chunk_size = chunk_size
        self.transcriber = transcriber
        self.transcriber_callback = transcriber_callback

    def record(self):
        """
        Start recording audio from the microphone.

        This method initializes the audio stream and starts a separate thread
        for continuously recording audio data.
        """
        try:
            if not self.listen_active:
                self.record_active = True
                self.audio_buffer = []
                self.stream = self.audio.open(
                    format=pyaudio.paInt16,
                    channels=1,
                    rate=16000,
                    input=True,
                    frames_per_buffer=self.chunk_size,
                )
                self.recorder_thread = threading.Thread(target=self._record)
                self.recorder_thread.start()
            else:
                log_error("Cannot start recording while listening.")
        except Exception as e:
            log_error(f"Error starting recording: {str(e)}")

    def listen(self):
        """
        Start listening for voice activity from the microphone.

        This method initializes the audio stream and starts a separate thread
        for detecting voice activity based on the specified threshold and pause length.
        When voice activity is detected, the audio data is stored for transcription.
        """
        try:
            if not self.record_active:
                self.listen_active = True
                self.stream = self.audio.open(
                    format=pyaudio.paInt16,
                    channels=1,
                    rate=16000,
                    input=True,
                    frames_per_buffer=self.chunk_size,
                )
                self.listener_thread = threading.Thread(target=self._listen)
                self.listener_thread.start()
            else:
                log_error("Cannot start listening while recording.")
        except Exception as e:
            log_error(f"Error starting listening: {str(e)}")

    def stop(self):
        """
        Stop the current recording or listening session.

        This method stops the audio stream, saves the recorded audio data to a file
        (if recording), and optionally transcribes the audio data (if listening).
        """
        try:
            if self.record_active:
                self.record_active = False
                if self.recorder_thread.is_alive():
                    self.recorder_thread.join()
                self.stream.stop_stream()
                self.stream.close()
                self._save_audio_file()
                return self.audio_file
            elif self.listen_active:
                self.listen_active = False
                if self.listener_thread.is_alive():
                    self.listener_thread.join()
                self.stream.stop_stream()
                self.stream.close()
            else:
                log_error("No active recording or listening.")
        except Exception as e:
            log_error(f"Error stopping recording/listening: {str(e)}")

    def _record(self):
        try:
            while self.record_active:
                data = self.stream.read(self.chunk_size, exception_on_overflow=False)
                self.audio_buffer.append(data)
        except Exception as e:
            log_error(f"Error recording audio: {str(e)}")

    def _listen(self):
        try:
            self.audio_buffer = []
            last_active_time = time.time()
            while self.listen_active:
                data = self.stream.read(self.chunk_size)
                volume = self._get_volume(data)
                if volume > self.threshold + self.background:
                    if not self.voice_active:
                        self.voice_active = True
                        log_info("Activation detected.")
                    self.audio_buffer.append(data)
                    last_active_time = time.time()
                else:
                    if (
                        self.voice_active
                        and time.time() - last_active_time > self.pause_length
                    ):
                        self.voice_active = False
                        log_warning("Activation ended.")
                        audio_data = self._prepare_audio_data(self.audio_buffer)
                        if self.transcriber:
                            threading.Thread(
                                target=self._transcribe, args=(audio_data,)
                            ).start()
                        else:
                            log_error("No transcriber set")
                        self.audio_buffer.clear()
        except Exception as e:
            log_error(f"Error listening for audio: {str(e)}")

    def set_transcriber(self, transcriber):
        self.transcriber = transcriber

    def set_transcription_callback(self, callback):
        self.transcription_callback = callback

    def _transcribe(self, audio_data):
        try:
            self.transcriber.transcribe(audio_data, self.transcription_callback)
        except Exception as e:
            log_error(f"Error transcribing audio: {str(e)}")

    def get_db_level(self):
        try:
            frames = []
            stream = self.audio.open(
                format=pyaudio.paInt16,
                channels=1,
                rate=16000,
                input=True,
                frames_per_buffer=self.chunk_size,
            )
            start_time = time.time()

            while time.time() - start_time < 1.5:
                data = stream.read(self.chunk_size)
                frames.append(data)

            stream.stop_stream()
            stream.close()

            return self._get_volume(b"".join(frames))
        except Exception as e:
            log_error(f"Error getting dB level: {str(e)}")

    def _prepare_audio_data(self, frames):
        audio_data = b"".join(frames)
        audio_np = (
            np.frombuffer(audio_data, np.int16).flatten().astype(np.float32) / 32768.0
        )
        return audio_np

    def _get_volume(self, data):
        """Convert the audio samples to decibels (dB) for a logarithmic representation of the volume"""
        samples = np.frombuffer(data, dtype=np.int16)
        normalized_samples = samples / 32768.0
        return 20 * np.log10(np.mean(np.abs(normalized_samples))) + 60

    def get_background(self):
        return self.background

    def set_background(self, background):
        self.background = background

    def get_threshold(self):
        return self.threshold

    def set_threshold(self, threshold):
        self.threshold = threshold

    def _save_audio_file(self):
        try:
            wf = wave.open(self.audio_file, "wb")
            wf.setnchannels(1)
            wf.setsampwidth(self.audio.get_sample_size(pyaudio.paInt16))
            wf.setframerate(16000)
            wf.writeframes(b"".join(self.audio_buffer))
            wf.close()
        except Exception as e:
            log_error(f"Error saving audio file: {str(e)}")
