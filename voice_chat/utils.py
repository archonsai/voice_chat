import logging
from logging.handlers import RotatingFileHandler

# Configure logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Set up file handler
file_handler = RotatingFileHandler(
    "voice_chat_debug.log", maxBytes=1024 * 1024, backupCount=1
)
file_handler.setLevel(logging.INFO)
file_formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
file_handler.setFormatter(file_formatter)

# Set up console handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_formatter = logging.Formatter("%(levelname)s: %(message)s")
console_handler.setFormatter(console_formatter)

# Add handlers to the logger
logger.addHandler(file_handler)
logger.addHandler(console_handler)


# Custom logging functions with colored output
def log_user(message):
    logger.info(f"{ascii_colors.OKCYAN}{message}{ascii_colors.ENDC}")


def log_assistant(message):
    logger.info(f"{ascii_colors.OKGREEN}{message}{ascii_colors.ENDC}")


def log_info(message):
    logger.info(message)


def log_error(message):
    logger.error(f"{ascii_colors.FAIL}{message}{ascii_colors.ENDC}")


def log_warning(message):
    logger.warning(f"{ascii_colors.WARNING}{message}{ascii_colors.ENDC}")


class ascii_colors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


conversation_history = []
