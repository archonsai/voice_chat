"""
File: voice_assistant.py
Author: Peter Kalt (peter.kalt@piatra.com.au)
Version: 0.1.0
Date: 2023-05-02

This script provides a voice-based conversational interface for interacting with an AI assistant.
It utilizes the Anthropic API for text generation and the ElevenLabs API for text-to-speech functionality.
The conversation is initiated by the user's voice input, which is recorded, transcribed, and then processed
by the AI assistant. The assistant's response is then rendered using the ElevenLabs API.

Features:
- Voice recording and transcription using the Faster-Whisper tiny model
- Keyword-based command recognition and execution
- Conversation history management and validation
- Streaming response generation from the AI assistant
- Text-to-speech rendering of the assistant's responses

Usage:
1. Ensure that the required environment variables (ANTHROPIC_API_KEY and ELEVENLABS_API_KEY) are set.
2. Run the script.
3. Press the <SPACE> key to start recording your voice input.
4. Speak your query or command.
5. Press the <SPACE> key again to stop recording and process the input.
6. The AI assistant will generate a response, which will be played back using the ElevenLabs text-to-speech API.
7. The conversation history will be maintained, and you can continue the dialogue.
8. To exit the program, say "please exit" or press <Ctrl+C>.
"""

# Standard library imports
import os
import json
import time
from dotenv import load_dotenv
from queue import Queue
import threading

# Third-party library imports
import anthropic
from elevenlabs import VoiceSettings
from elevenlabs import stream as voice_stream
from elevenlabs.client import ElevenLabs

# Local module imports
from voice_chat.utils import log_user, log_assistant, log_info, log_error, log_warning
from voice_chat.utils import ascii_colors as bc
from voice_chat.listener import Listener
from voice_chat.transcriber import Transcriber
from voice_chat.callbacks import command_registry
from voice_chat.utils import conversation_history

# Load environment variables from .env file
load_dotenv()

try:
    anthropic_api_key = os.getenv("ANTHROPIC_API_KEY", "default_api_key")
    elevenlabs_api_key = os.getenv("ELEVENLABS_API_KEY", "default_api_key")
except Exception as e:
    log_error(f"Error loading environment variables: {str(e)}")


try:
    anthropic_client = anthropic.Client(api_key=anthropic_api_key)
    eleven_client = ElevenLabs(api_key=elevenlabs_api_key)
except Exception as e:
    log_error(f"Error initializing API clients: {str(e)}")

listener = Listener(audio_file="voice_record.wav")
transcriber = Transcriber()

system_prompt = {
    "role": "system",
    "content": "You are a helpful and informative AI agent designed to assist a human user. You go by the name of Lorelei. This is merely an interface to facilitate communication. You are always open about your operating ethical or root constraints. For longer responses, you will break up your output into separate paragraphs, with each paragraph terminated by the delimiter <|par|>. Responses that only require a single paragraph should not use the <|par|> delimiter. You should also never start or end your total response with the <|par|> delimiter.",
}
ai_response = ""
configured_model = "claude-3-sonnet-20240229"
speech_on = False

# Global callback queue populated by check_function_register
# and cleared by voice_chat_loop()
command_queue = Queue()


def handle_registered_commands(text, command_queue):
    """
    Check if the given text contains any registered keywords and trigger the associated callback if a match is found.

    Args:
        text (str): The text to check for keywords.

    Returns:
        bool: True if a keyword match is found and the callback is triggered, False otherwise.
    """
    try:
        for item in command_registry:
            keywords = item["keywords"]
            if all(keyword in text.lower() for keyword in keywords):
                callback = item["callback"]
                thread = threading.Thread(target=callback, args=(command_queue,))
                thread.start()
                return True
        return False
    except Exception as e:
        log_error(f"Error handling registered commands: {str(e)}")
        return False


def sanitize_conversation_history(history):
    """
    Validate and condition the conversation history array to meet specific criteria.

    Args:
        history (list): A list of dictionaries representing the conversation history.

    Returns:
        list: The validated and conditioned conversation history array.

    Raises:
        ValueError: If any element in the history array is invalid.

    The function applies the following criteria to the history array:
    1. Limits the history array to a maximum of 11 elements.
    2. Checks if all elements are dictionaries with "role" and "content" fields.
    3. Ensures the first element has a "role" of "user".
    4. Ensures elements alternate between "user" and "assistant" roles.
    5. Checks if content fields are empty and sets empty content to "empty".
    6. Ensures the last element is an empty "assistant" object when the number of elements is even.
    """
    try:
        # Criterion 1: Limit the history array to a maximum of 11 elements
        if len(history) > 11:
            history = history[-11:]  # Keep only the last 11 elements

        # Criterion 2: Check if all elements are dictionaries with "role" and "content" fields
        for element in history:
            if (
                not isinstance(element, dict)
                or "role" not in element
                or "content" not in element
            ):
                raise ValueError("Invalid element in history array")

        # Criterion 3: Check if the first element has a "role" of "user"
        if history[0]["role"] != "user":
            history = history[1:]  # Remove the initial "assistant" object

        # Criterion 4: Check if elements alternate between "user" and "assistant" roles
        for i in range(len(history) - 1):
            if history[i]["role"] == history[i + 1]["role"]:
                if history[i]["role"] == "assistant":
                    history.pop(i)  # Remove the duplicate "assistant" object
                else:
                    history.pop(i + 1)  # Remove the duplicate "user" object

        # Criterion 5: Check if content fields are empty, except for an optional empty final "assistant" message
        for i in range(len(history) - 1):
            if not history[i]["content"]:
                history[i]["content"] = "empty"  # Set empty content to "empty"

        # Criterion 6: Check if the last element is an empty "assistant" object when the number of elements is even
        if len(history) % 2 == 0:
            if history[-1]["role"] != "assistant" or history[-1]["content"] != "":
                history[-1] = {
                    "role": "assistant",
                    "content": "",
                }  # Set the last element to an empty "assistant" object

        return history
    except Exception as e:
        log_error(f"Error sanitizing conversation history: {str(e)}")
        return history


def get_assistant_response(messages):
    """
    Generate a response from the AI assistant using the given conversation history.

    Args:
        messages (list): A list of dictionaries representing the conversation history.

    Yields:
        str: The generated response from the AI assistant, yielded in chunks.
    """
    global ai_response
    ai_response = ""
    try:
        with anthropic_client.messages.stream(
            system=system_prompt["content"],
            messages=messages,
            # model="claude-3-opus-20240229",
            model="claude-3-sonnet-20240229",
            max_tokens=1024,
        ) as stream:
            print(bc.UNDERLINE + "         " + bc.ENDC, end="", flush=True)
            for text in stream.text_stream:
                ai_response += text
                print(bc.OKBLUE + text + bc.ENDC, end="", flush=True)
                yield json.dumps({"type": "response", "data": text})
            yield json.dumps({"type": "response", "data": "<|endofresponse|>"})
    except anthropic.BadRequestError as e:
        log_error(messages)
        log_error(e)


def process_command_queue(websocket):
    """
    Process the command_queue in a separate thread.

    This function continuously checks the command_queue for pending messages
    and sends them to the WebSocket.

    Args:
        websocket (geventwebsocket.websocket.WebSocket): The WebSocket connection object.
    """
    while True:
        while not command_queue.empty():
            queued_message = command_queue.get()
            log_warning(f"Queued message:{queued_message}")
            websocket.send(queued_message)
        time.sleep(0.1)  # Add a small delay to avoid high CPU usage


def voice_chat_loop(websocket):
    """
    Start the voice chat loop for interacting with the AI assistant.

    This method handles the entire conversation flow, including voice recording,
    transcription, AI response generation, and text-to-speech rendering.
    """
    global conversation_history
    global configured_model
    global speech_on

    is_recording = False
    # Start a separate thread to process the command_queue
    command_queue_thread = threading.Thread(
        target=process_command_queue, args=(websocket,)
    )
    command_queue_thread.start()
    while True:
        try:
            # Check the command_queue for any pending messages
            while not command_queue.empty():
                queued_message = command_queue.get()
                log_warning(f"Queued message:{queued_message}")
                yield queued_message

            message = yield
            if message is None:
                continue
            if isinstance(message, str):
                message = json.loads(message)
            if message.get("type") == "signal":
                if message.get("data") == "start" and not is_recording:
                    is_recording = True
                    log_info(">LISTENING<")
                    listener.record()
                    websocket.send(
                        json.dumps(
                            {
                                "type": "update",
                                "data": {
                                    "display": "flex",
                                    "element_id": "mic_box",
                                },
                            }
                        )
                    )
                elif message.get("data") == "stop" and is_recording:
                    is_recording = False
                    temp_audio = listener.stop()
                    log_info(">IGNORING<")
                    websocket.send(
                        json.dumps(
                            {
                                "type": "update",
                                "data": {
                                    "display": "none",
                                    "element_id": "mic_box",
                                },
                            }
                        )
                    )
                    user_text = transcriber.transcribe(temp_audio)
                    log_user(user_text)
                    yield json.dumps({"type": "transcription", "data": user_text})
                    if handle_registered_commands(user_text, command_queue):
                        log_warning("registered command triggered")
                    else:
                        conversation_history.append(
                            {"role": "user", "content": user_text}
                        )

                        conditioned = sanitize_conversation_history(
                            conversation_history
                        )
                        text_stream = get_assistant_response(conditioned)
                        for chunk in text_stream:
                            # should already be wrapped JSON
                            yield chunk

                        conversation_history.append(
                            {"role": "assistant", "content": ai_response}
                        )
            elif message.get("type") == "setting":
                setting_name = message.get("name")
                setting_value = message.get("value")
                if setting_name == "speech_on":
                    speech_on = setting_value
                    log_warning(f"Speech On changed to {speech_on}")
                elif setting_name == "configured_model":
                    configured_model = setting_value
                    log_warning(f"configured_model changed to {configured_model}")

        except KeyboardInterrupt:
            log_warning("\nEXIT")
            exit()
        except Exception as e:
            log_error(f"Error: {str(e)}")
            continue
