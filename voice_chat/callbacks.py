import os
import json
import random
import uuid
import time

from voice_chat.utils import log_warning, log_error, log_info
from voice_chat.utils import conversation_history

vault_file = "vault.txt"
log_file = "voice_chat_debug.log"


# JUNK FUNCTIONS IGNORE


def roll_and_track_histograms():
    first_roll_histogram = [0] * 20
    median_histogram = [0] * 20

    for _ in range(10000):
        rolls = [random.randint(1, 20) for _ in range(3)]
        first_roll_histogram[rolls[0] - 1] += 1
        rolls.sort()
        median_histogram[rolls[1] - 1] += 1

    print("First Roll Histogram:")
    render_histogram(first_roll_histogram)
    print("\nMedian Roll Histogram:")
    render_histogram(median_histogram)


def render_histogram(histogram):
    max_count = max(histogram)
    scale_factor = 20 / max_count if max_count > 0 else 1

    for i, count in enumerate(histogram):
        scaled_count = int(count * scale_factor)
        print(f"{i + 1:2d}: {'#' * scaled_count}{' ' * (20 - scaled_count)}")


# REAL FUNCTIONS


def send_create_packet(callback_queue, label, element_id=str(uuid.uuid4())):
    packet = json.dumps(
        {"type": "create", "data": {"element_id": element_id, "label": label}}
    )
    log_error(packet)
    callback_queue.put(packet)


def send_update_packet(callback_queue, data_dict, element_id):
    data = {"element_id": element_id, **data_dict}
    packet = json.dumps({"type": "update", "data": data})
    log_error(packet)
    callback_queue.put(packet)


# Callback functions
def SaveVaultCallback(callback_queue):
    element_id = str(uuid.uuid4())
    log_info("Saving Vault")
    send_create_packet(
        callback_queue, "SaveVaultCallback(): Saving Vault...", element_id
    )


# Callback functions
def DeleteVaultCallback(callback_queue):
    element_id = str(uuid.uuid4())
    log_info("Deleting Vault")
    send_create_packet(
        callback_queue, "DeleteVaultCallback(): Deleting Vault...", element_id
    )
    confirm = input("Are you sure?")
    if confirm.lower().startswith("y"):
        if os.path.exists(vault_file):
            os.remove(vault_file)
            log_info("Vault deleted")
            vault_content = []
            vault_embeddings = []
            vault_embeddings_tensor = []
            send_update_packet(
                callback_queue,
                {
                    "textContent": "DeleteVaultCallback(): Vault deleted OK",
                    "className": "process-bubble pass",
                },
                element_id,
            )
        else:
            log_error("Vault already empty")
            send_update_packet(
                callback_queue,
                {
                    "textContent": "DeleteVaultCallback(): Vault already empty",
                    "className": "process-bubble fail",
                },
                element_id,
            )


# Callback functions
def PrintVaultCallback(callback_queue):
    element_id = str(uuid.uuid4())
    log_info("Vault contents:")
    send_create_packet(
        callback_queue, "PrintVaultCallback(): Printing Vault...", element_id
    )
    if os.path.exists(vault_file):
        with open(vault_file, "r", encoding="utf-8") as vault_x:
            log_info(vault_x.read())
    else:
        log_error("Vault is empty")
        send_update_packet(
            callback_queue,
            {
                "textContent": "PrintVaultCallback(): Vault is empty",
                "className": "process-bubble fail",
            },
            element_id,
        )


# Callback functions
def PrintLogCallback(callback_queue):
    element_id = str(uuid.uuid4())
    log_info("Log contents:")
    send_create_packet(
        callback_queue, "PrintVaultCallback(): Printing Vault...", element_id
    )
    try:
        with open(log_file, "r") as file:
            lines = file.readlines()
            if len(lines) >= 5:
                result = "\n".join(lines[-5:])
            else:
                result = "\n".join(lines)
            send_update_packet(
                callback_queue,
                {
                    "textContent": f"PrintVaultCallback():\n{result}",
                    "className": "process-bubble fail",
                },
                element_id,
            )
    except FileNotFoundError:
        log_error(f"FileNotFoundError:{log_file}")
        send_update_packet(
            callback_queue,
            {
                "textContent": f"PrintVaultCallback():\nNot found {log_file}",
                "className": "process-bubble fail",
            },
            element_id,
        )
    except Exception as e:
        log_error(f"Error reading file:{log_file}")
        send_update_packet(
            callback_queue,
            {
                "textContent": f"PrintVaultCallback():\nError reading {log_file}",
                "className": "process-bubble fail",
            },
            element_id,
        )


def AbortCallback(callback_queue):
    element_id = str(uuid.uuid4())
    log_warning("Exiting")
    send_create_packet(callback_queue, "AbortCallback(): Exiting...", element_id)


def TestCallback(callback_queue):
    log_info("TESTING")
    element_id = str(uuid.uuid4())
    send_create_packet(callback_queue, "TestCallback(): Starting...", element_id)

    # Simulate a long-running process
    for i in range(5):
        roll_and_track_histograms()
        send_update_packet(
            callback_queue,
            {
                "textContent": f"TestCallback(): Progress {i+1}/5",
                "className": "process-bubble",
            },
            element_id,
        )
        time.sleep(1)  # Simulate some processing time

    send_update_packet(
        callback_queue,
        {"textContent": "TestCallback(): Finished", "className": "process-bubble pass"},
        element_id,
    )


def ShowHistoryCallback(command_queue):
    element_id = str(uuid.uuid4())
    send_create_packet(
        command_queue,
        "ShowHistoryCallback(): Showing recent conversation history...",
        element_id,
    )
    if len(conversation_history) == 0:
        log_warning("Conversation history is empty")
        send_update_packet(
            command_queue,
            {
                "textContent": "Conversation history is empty",
                "className": "process-bubble fail",
            },
            element_id,
        )
        return "Happy"

    recent_history = conversation_history[-4:]

    for index, message in enumerate(recent_history, start=1):
        role = message["role"]
        content = message["content"]

        send_update_packet(
            command_queue,
            {
                "textContent": f"Message {index}:\nRole: {role}\nContent: {content}",
                "className": "process-bubble",
            },
            element_id,
        )
        time.sleep(0.3)

    send_update_packet(
        command_queue,
        {
            "textContent": "ShowHistoryCallback(): Recent conversation history displayed",
            "className": "process-bubble pass",
        },
        element_id,
    )

    return "Happy"


# Function registry
command_registry = [
    {
        "keywords": ["please", "save", "chat"],
        "callback": SaveVaultCallback,
    },
    {
        "keywords": ["please", "show", "history"],
        "callback": ShowHistoryCallback,
    },
    {
        "keywords": ["please", "print", "chat"],
        "callback": PrintVaultCallback,
    },
    {
        "keywords": ["please", "print", "log"],
        "callback": PrintLogCallback,
    },
    {
        "keywords": ["please", "delete", "chat"],
        "callback": DeleteVaultCallback,
    },
    {"keywords": ["please", "exit"], "callback": AbortCallback},
    {
        "keywords": ["please", "test"],
        "callback": TestCallback,
    },
]
