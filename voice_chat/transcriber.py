"""
This module provides a class for transcribing audio data to text using the Faster Whisper model.
"""

import faster_whisper

from voice_chat.utils import log_info, log_error, log_warning


class Transcriber:
    def __init__(self, model_name="tiny.en", callback=None):
        """
        Initialize the Transcriber object.

        Args:
            model_name (str): Name or path of the Faster Whisper model to use.
            callback (callable): Optional callback function to handle the transcription result.
        """
        try:
            self.model = faster_whisper.WhisperModel(
                model_size_or_path=model_name, device="cpu"
            )
        except Exception as e:
            log_error(f"Error loading Whisper model: {str(e)}")
        self.callback = callback

    def transcribe(self, audio_data, callback=None):
        """
        Transcribe the given audio data to text.

        Args:
            audio_data (numpy.ndarray): Audio data as a numpy array.
            callback (callable): Optional callback function to handle the transcription result.

        Returns:
            str: The transcribed text if no callback is provided.
        """
        try:
            segments, info = self.model.transcribe(audio_data, beam_size=5)
            transcription = ""
            for segment in segments:
                transcription += segment.text + " "
            if callback:
                callback(transcription.strip())
            else:
                return transcription.strip()
        except Exception as e:
            log_error(f"Error transcribing audio: {str(e)}")
